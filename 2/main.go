package main

import "fmt"

func genFibs() <-chan int {
	c := make(chan int)
	go func() {
		defer close(c)
		res := 0
		i := 0
		for res < 4000000 {
			i++
			res = fibonacci(i)
			if res%2 == 0 {
				c <- res
			}
		}
	}()
	return c
}

func fibonacci(num int) int {
	if num <= 1 {
		return num
	}
	return fibonacci(num-1) + fibonacci(num-2)
}

func main() {
	sum := 0
	for v := range genFibs() {
		sum += v
	}
	fmt.Println("Final result:", sum)
}
